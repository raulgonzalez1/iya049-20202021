level0 ={
    "level":[1,1,1,1,1,1,1,1,1,2,2,2,2,2,3,1,1,2,2,2,2,2,3,1,1,2,2,2,2,2,3,1,1,1,1,1,1,1,1,1],
    "size":[5,8],
    "start":[2,1],
    "box":[1,4,2,4,3,4]
}
level1 = {
    "level":[1,1,1,1,1,1,1,2,3,3,3,1,1,2,2,2,2,1,1,2,2,2,1,1,1,2,2,2,1,1,1,1,1,1,1,1],
    "size":[6,6],
    "start":[2,4],
    "box":[2,3,3,2,3,3]
};
level3 = {
    "level":[1,1,1,1,1,1,1,1,3,2,2,2,3,1,1,2,2,2,2,2,1,1,2,2,2,2,2,1,1,3,2,2,2,3,1,1,3,3,2,3,3,1,1,1,1,1,1,1,1],
    "size":[7,7],
    "start":[3,3],
    "box":[2,2,2,3,2,4,3,2,3,4,4,2,4,3,4,4]
};
level4 = {
    "level":[1,1,1,1,1,1,1,1,1,1,2,2,2,2,3,3,3,1,1,2,1,2,1,3,1,3,1,1,2,1,2,2,3,3,3,1,1,2,2,2,2,2,1,2,1,1,2,2,2,2,2,2,2,1,1,2,2,2,2,1,1,2,1,1,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1],
    "size":[9,9],
    "start":[5,3],
    "box":[4,2,4,3,4,4,5,2,5,4,6,2,6,3,6,4]
};
level2 ={
    "level":[1,1,1,1,1,1,1,1,1,3,2,2,2,2,3,1,1,3,2,2,2,2,3,1,1,3,2,2,2,2,3,1,1,1,1,1,1,1,1,1],
    "size":[5,8],
    "start":[3,3],
    "box":[1,4,2,2,2,3,2,4,2,5,3,4]
}
level5 ={
    "level":[1,1,1,1,1,1,1,1,2,3,2,3,2,1,1,2,2,1,2,2,1,1,2,2,3,2,2,1,1,1,2,2,2,1,1,1,1,2,2,2,1,1,1,1,1,1,1,1,1],
    "size":[7,7],
    "start":[1,5],
    "box":[1,4,2,4,4,4]
}
var charpos=[];
var boxes=[];
var game=[];
var currentLevel;
var levelname;
var win=false;
var levelnumber;
var moves=0;
var username="Guest";
var userscores = ""

function changeUsername(name){
    username = name;
}
function movement(e){
    switch(e.key.toLowerCase()){
        case "a":
            if(checkMove(0 ,-1)){
                moveChar(0 ,-1);
            }
            break;
        case "s":
            if(checkMove(1,0)){
                moveChar(1,0);
            }
            break;        
        case "d":
            if(checkMove(0,1)){
                moveChar(0,1);
            }
            break;
        case "w":
            if(checkMove(-1,0)){
                moveChar(-1,0);
            }
            break;
    }
    win=checkWin();
}
var checkWin = () =>{
    var wincondition = true;
    for (var i=0;i<boxes.length/2;i++){
        if(game[boxes[i*2]][boxes[[(i*2)+1]]]!=3){
            wincondition=false;
            break;
        }
    }
    if (wincondition){
        var req = new XMLHttpRequest();
        var params = moves + " " + levelname + " " + username;
        req.open('POST', '', true);
        req.onreadystatechange = function (e) {
            if (req.readyState == 4) {
                userscores=JSON.parse(req.responseText);
            }
        };
        req.send(params); 
    }
    return wincondition;
}
var checkMove = (x,y) =>{
    if(game[charpos[0]+x][charpos[1]+y]!=1){
        for(var i=0;i<boxes.length/2;i++){
            if (boxes[i*2]==(charpos[0]+x) && boxes[(i*2)+1]==(charpos[1]+y))
            {
                if(game[charpos[0]+(x*2)][charpos[1]+(y*2)]!=1){
                    for(var j=0;j<boxes.length/2;j++){
                        if (boxes[j*2]==(charpos[0]+x*2) && boxes[(j*2)+1]==(charpos[1]+y*2)){
                            return false;
                        }
                    }
                    moveBox(i,x,y);
                    return true;
                }
                else{return false;}
            }
        }
        return true;
    }
    return false;
}
var moveBox = (i,x,y) =>{
    boxes[i*2]+=x;
    boxes[(i*2)+1]+=y;
}
var moveChar = (x,y) =>{
    moves++;
    charpos[0]+=x;
    charpos[1]+=y;
}
var createGameArray = (x,y) =>{
    var array2d = new Array(x); 
    for (var i = 0; i < array2d.length; i++) {
        array2d[i] = new Array(y); 
    } 
    return array2d;
};
var fillGameArray = (array2d) =>{
    var x=0;
    for (var i = 0; i < array2d.length; i++) { 
        for (var j = 0; j < array2d[i].length; j++) { 
            array2d[i][j] = currentLevel.level[x]; 
            x++;
        } 
    }
    return array2d;
};
var printGameArray = (array2d) =>{
    var content = "";
    var printed = false;
    if(win){
        content +=`<div class="win">LEVEL COMPLETE</div>`
    }
    for (var i = 0; i < array2d.length; i++) { 
        for (var j = 0; j < array2d[i].length; j++) { 
            printed = false;
            if (charpos[0]==i && charpos[1]==j){
                content+='<div class="gameobject character"></div>' + " ";
                continue;
            }
            for(var k=0; k<boxes.length/2 ;k++){
                if (boxes[k*2]==i && boxes[(k*2)+1]==j){
                    if(array2d[i][j]==3){
                        content+='<div class="gameobject boxcorrect"></div>'+ " ";
                    }
                    else{
                    content+='<div class="gameobject box"></div>'+ " ";
                    }
                    printed=true;
                    break;
                }
            }
            if(!printed){
                switch(array2d[i][j]){
                    case 1:
                        content+='<div class="gameobject wall"></div>' + " ";
                        break;
                    case 2:
                        content+='<div class="gameobject empty"></div>' + " ";
                        break;
                    case 3:
                        content+='<div class="gameobject goal"></div>' + " "; 
                        break;
                }
            }
        } 
        content +='<div class="nextline"></div>'; 
    }
    return content;
};
var selectLevel = (x) =>{
    levelnumber=parseInt(x);
    switch(x){
        case "0":
            levelname = "level0";
            return JSON.parse(JSON.stringify(level0));
        case "1":
            levelname = "level1";
            return JSON.parse(JSON.stringify(level1));
        case "2":
            levelname = "level2";
            return JSON.parse(JSON.stringify(level2));
        case "3":
            levelname = "level3";
            return JSON.parse(JSON.stringify(level3));
        case "4":
            levelname = "level4";
            return JSON.parse(JSON.stringify(level4));
        case "5":
            levelname = "level5";
            return JSON.parse(JSON.stringify(level5));
    }
    return null;
}
function start(x){
    currentLevel=selectLevel(x);
    game = createGameArray(currentLevel.size[0],currentLevel.size[1]);
    charpos = currentLevel.start;
    moves=0;
    win = false;
    boxes = currentLevel.box;
    game = fillGameArray(game);
    var content = printGameArray(game);
    return content;
}
function getUserscores(){
    var usertable ="";
    var levelstring;
    for(var [key,value]of Object.entries(userscores)){
        switch(key){
            case "level0":
                levelstring="LEVEL 0";
                break;
            case "level1":
                levelstring="LEVEL 1";
                break;
            case "level2":
                levelstring="LEVEL 2";
                break;
            case "level3":
                levelstring="LEVEL 3";
                break;
            case "level4":
                levelstring="LEVEL 4";
                break;
            case "level5":
                levelstring="LEVEL 5";
                break;
        }
        usertable += `<hr><div class="score">${levelstring}  -  ${value} Moves</div>`;
    }
    usertable +="";
    return usertable;
}
function updateGameState(){
    var content = printGameArray(game);
    return content;
}
function currentLvl(){
    return levelnumber;
}