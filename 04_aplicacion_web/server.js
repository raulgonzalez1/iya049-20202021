const fs = require('fs');
const createServer = require("./create_server.js");
var highscores = {
  "Test": {
    "level0":"10",
    "level1":"11",
    "level2":"12",
    "level3":"13",
    "level4":"14",
    "level5":"15",
  }
 }

const get = (request, response) => {
  if (request.path == '/')  {
    body=fs.readFileSync(__dirname+'/sokoban.html', 'utf8');
    headers='{ "Content-Type": "text/html" }'
  } else {
    body=fs.readFileSync(__dirname+request.path, 'utf8');
    format = request.path.split(".")[1];
    switch(format){
      case('html'):
        headers='{ "Content-Type": "text/html" }'
        break;
      case('css'):
        headers='{ "Content-Type": "text/css" }'
        break;
      case('js'):
        headers='{ "Content-Type": "text/javascript" }' 
    }
  }
  response.send(
      "200",
      headers,
      body
    );
};
const requestListener = (request, response) => {
  switch (request.method) {
    case "GET": {
      return get(request, response);
    }
    case "POST": {
      username = request.body.split(" ")[2];
      level = request.body.split(" ")[1];
      moves = request.body.split(" ")[0];
      if(highscores.hasOwnProperty(username)){
        if(highscores[username].hasOwnProperty(level)){
          if (parseInt(highscores[username][level]) > parseInt(moves)){
            highscores[username][level] = moves;
          }
        } else {
          highscores[username][level] = moves;
        }
      } else {
        highscores[`${username}`] = {};
        highscores[username][`${level}`] = moves;
      }
      var body=JSON.stringify(highscores[username]);
      return response.send(
        202,
        { "Content-Type": "text/plain" },
        body
      );
    }
    default: {
      return response.send(
        404,
        { "Content-Type": "text/plain" },
        "Unsupported method"
      );
    }
  }
};

const server = createServer((request, response) => {
  try {
    return requestListener(request, response);
  } catch (error) {
    console.error(error);
    response.send(500, { "Content-Type": "text/plain" }, "Uncaught error");
  }
});
server.listen(3000);
//server.close();