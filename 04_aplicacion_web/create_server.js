const events = require("events");
const net = require("net");
const splitHeaders = (h) => {
  let data = new Object();
  h.split("\r\n").forEach(line => {
    let parts = line.split(": ");
    data[parts[0].toUpperCase()] = parts[1];
  });
  return data;
}
const request = () => {
  let request = new Object();
  request.method = null;
  request.body = null;
  request.path = null;
  request.headers = new Object();
  request.getHeader = (header) => request.headers[header.toUpperCase()] === undefined ? null : request.headers[header.toUpperCase()];
  request.setHeader = (key, val) => request.headers[key.toUpperCase()] = val;
  request.setHeaders = (obj) => request.headers = obj;
  return request;
};

const createServer = (requestHandler) => {
  const server = net.createServer(socket => {
    console.log('server connected');
    let segmentado = false;
    let req;
    socket.on('data', function (data) {
      let chunk = data.toString();
      req = request();
      var d_index = chunk.indexOf('\n\r');
      req.method = chunk.split(" ")[0];
      req.path = chunk.split(" ")[1];
      req.setHeaders(splitHeaders(chunk.substring(0, d_index - 1)));
      d_index += 3;
      if (req.getHeader('CONTENT-LENGTH') !== null) {
        req.body = chunk.substring(d_index, d_index + parseInt(req.getHeader('CONTENT-LENGTH')));
        if (req.body.length < req.getHeader('CONTENT-LENGTH')) {
          segmentado = true;
          return;
        }
      } else {
        req.body = chunk.substring(d_index, chunk.length);
      }
      if (segmentado) {
        req.body += chunk;
      }
      requestHandler(req, {
        send: (code, headers, body) => {
          headers['Content-Length'] = body.length;
          headers['Date'] = (new Date()).toUTCString();
          socket.write(`HTTP/1.1 ${code} CodeMessage\r\n`);
          Object.entries(headers).forEach(([key, value]) => {
            socket.write(`${key}: ${value}\r\n`);
          });
          socket.end(`\r\n${body}\r\n`);
          socket.destroy();
        }
      });
    });
    socket.on('error', function (e) {
      if (e.code == 'EADDRINUSE') {
        console.log('Address in use, retrying...');
        setTimeout(function () {
          socket.close();
          socket.listen(PORT, HOST);
        }, 1000);
      }
    });
    socket.setTimeout(1000);
    socket.on('timeout', () => {
      console.log('socket timeout');
      socket.end();
    });
    socket.on('end', function () {
      console.log('server disconnected');
    });
  });
  return {
    listen: (portNumber) => {
      console.log('opened server on http://localhost:' + portNumber);
      server.listen(portNumber);
    },
    close: () => {
      server.close();
    },
  };
};
module.exports = createServer;